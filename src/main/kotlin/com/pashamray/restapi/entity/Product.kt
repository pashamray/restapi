package com.pashamray.restapi.entity

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "product")
data class Product(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "PROD_ID")
        val id: Int = 0,

//        @Column(name = "LANG_ID", nullable = false)
//        val lang_id: Int = 1,
//
        @Column(name = "PROD_NAME", unique = true, nullable = false, length = 255)
        val name: String = "",
//
//        @Column(name = "UNIT_ID", nullable = false, columnDefinition = "SMALLINT")
//        val unit_id: Int = 0,
//
        @Column(name = "PRICE_START", columnDefinition = "DECIMAL")
        val price_start: Float?,
//
//        @Column(name = "VAL_START", nullable = false, columnDefinition = "SMALLINT")
//        val val_start: Int = 0,
//
        @Column(name = "PRICE_SALE", columnDefinition = "DECIMAL")
        val price: Float?,
//
//        @Column(name = "VAL_SALE", nullable = false, columnDefinition = "SMALLINT")
//        val val_sale: Float = 0F,
//
//        @Column(name = "NACHENKA", columnDefinition = "DECIMAL")
//        val nachenka: Float?,
//
        @Column(name = "MODEL", length = 39)
        val model: String?,
//
//        @Column(name = "MAMUF_ID", columnDefinition = "SMALLINT")
//        val manuf_id: Int?,
//
//        @Column(name = "OTDEL_ID", nullable = false, columnDefinition = "SMALLINT")
//        val otdel_id: Int = 0,
//
//        @Column(name = "SKIDKA_USE", columnDefinition = "TINYINT")
//        val skidka_use: Boolean?,
//
//        @Column(name = "STOCK_USE", columnDefinition = "TINYINT")
//        val stock_use: Boolean?,
//
//        @Column(name = "FOTO_USE", columnDefinition = "TINYINT")
//        val foto_use: Boolean?,
//
        @Column(name = "REMARK", length = 254)
        val image: String?
//
//        @Column(name = "STATUS", columnDefinition = "TINYINT")
//        val status: Int?,
//
//        @Column(name = "FDELETE", nullable = false, columnDefinition = "TINYINT")
//        val fdelete: Boolean = false,
//
//        @Column(name = "USER_ADD")
//        val user_add: Int?,
//
//        @Column(name = "DATE_UPD")
//        val date_upd: Date?
)