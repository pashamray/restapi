package com.pashamray.restapi.controller

import com.pashamray.restapi.entity.Product
import com.pashamray.restapi.exception.ProductNotFoundException
import com.pashamray.restapi.repository.ProductRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/products")
class ProductController(val productRepository: ProductRepository) {

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody product: Product) = productRepository.save(product)

    @GetMapping("")
    fun findAll(
            @RequestParam(name = "offset", required = false, defaultValue = "0") offset: Int,
            @RequestParam(name = "limit", required = false, defaultValue = "100") limit: Int
    ): Page<Product> {
        println("offset: $offset")
        println("limit: $limit")
        return productRepository.findAll(PageRequest.of(offset / limit, limit))
    }

    @GetMapping("{id}")
    fun findById(@PathVariable id: Int) = productRepository.findById(id).orElseThrow { ProductNotFoundException(id) }

    @PutMapping("{id}")
    fun update(@PathVariable id: Int, @RequestBody product: Product) {
        if (!productRepository.existsById(id)) {
            throw ProductNotFoundException(id)
        }
        productRepository.save(product.copy(id = id))
    }

    @DeleteMapping("{id}")
    fun deleteById(@PathVariable id: Int) {
        if (!productRepository.existsById(id)) {
            throw ProductNotFoundException(id)
        }
        productRepository.deleteById(id)
    }
}