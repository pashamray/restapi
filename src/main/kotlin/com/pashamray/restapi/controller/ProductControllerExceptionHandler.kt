package com.pashamray.restapi.controller

import com.pashamray.restapi.exception.ErrorResponse
import com.pashamray.restapi.exception.ProductNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@ControllerAdvice
class ProductControllerExceptionHandler {
    @ResponseBody
    @ExceptionHandler(ProductNotFoundException::class)
    internal fun productExceptionHandler(ex: ProductNotFoundException): ResponseEntity<ErrorResponse> {
        val httpError = HttpStatus.NOT_FOUND
        val errorResponse = ErrorResponse(httpError.value(), httpError.reasonPhrase, ex.localizedMessage
                ?: "Requested Data Not Found")
        return ResponseEntity(errorResponse, httpError)
    }
}