package com.pashamray.restapi.repository

import com.pashamray.restapi.entity.Product
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository

interface ProductRepository: CrudRepository<Product, Int>, PagingAndSortingRepository<Product, Int>