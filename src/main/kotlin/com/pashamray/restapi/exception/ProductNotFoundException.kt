package com.pashamray.restapi.exception

class ProductNotFoundException(private val id: Int) : RuntimeException() {
    override val message: String?
        get() = "Could not find product $id"
}