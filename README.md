# Содержание
- [Получить список продуктов](#получить-список-продуктов)
- [Получить продукт по id](#получить-продукт-по-id)
- [Изменить продукт по id](#изменить-продукт-по-id)
- [Удалить продукт по id](#удалить-продукт-по-id)
- [Создать продукт](#создать-продукт)

### Получить список продуктов
Для этого необходимо выполнить `GET` запрос на `/api/products`
```
http://имя сервера/api/products
```
### Получить продукт по id
Для этого необходимо выполнить `GET` запрос на `/api/products/{id}`
- id: `{id}`
- URL: `http://имя сервера/api/products/{id}`
### Изменить продукт по id
Для этого необходимо выполнить `PUT` запрос на `/api/products/{id}`
- id: `{id}`
- URL: `http://имя сервера/api/products/{id}`
- Headers: `Content-Type: application/json`
- Body содержит поля которые надо изменить: 
    ```json
    {
	    "fdelete": 0,
	    "lang_id": 1,
	    "otdel_id": 0,
	    "prod_name": "test product 6",
	    "unit_id": 0,
	    "val_sale": 123,
	    "val_start": 1
    }
    ```
### Удалить продукт по id
Для этого необходимо выполнить `DELETE` запрос на `/api/products/{id}`
- id: `{id}`
- URL: `http://имя сервера/api/products/{id}`

### Создать продукт
Для этого необходимо выполнить `POST` запрос на `/api/products`
- URL: `http://имя сервера/api/products`
- Headers: `Content-Type: application/json`
- минимальный Body: 
    ```json
    {
        "prod_name": "уникальное имя"
    }
    ```